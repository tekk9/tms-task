package com.taskmanagementsystem.demo.controller;

import com.taskmanagementsystem.demo.dao.TaskRepository;
import com.taskmanagementsystem.demo.entity.Task;
import com.taskmanagementsystem.demo.utils.TaskFilter;
import com.taskmanagementsystem.demo.utils.TimeSpentFormatChecker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/task")
public class TaskRestController
{
    @Autowired
    private TaskRepository taskRepository;

    @GetMapping("/{id}")
    public ResponseEntity<Task> getTaskById(@PathVariable("id") Integer taskID)
    {
        try {
            Task task = taskRepository.findById(taskID).orElseGet(null);
            return ResponseEntity.ok(task);
        } catch(Exception e) {
            return ResponseEntity.status(404).body(null);
        }
    }

    @GetMapping
    public Iterable<Task> getAllTasks()
    {
        return taskRepository.findAll();
    }

    @DeleteMapping("id/{id}")
    public ResponseEntity<Task> deleteTask(@PathVariable("id") Integer id)
    {
        try{
            Task task = taskRepository.findById(id).orElseGet(null);
            taskRepository.delete(task);
            return ResponseEntity.ok(task);
        } catch(Exception e) {
            return ResponseEntity.status(404).body(null);
        }
    }

    @PostMapping
    public ResponseEntity<?> addNewTask(@RequestBody Task task)
    {

        if(TimeSpentFormatChecker.checkTimeSpentFormat(task.getTimeSpent()))
        {
            taskRepository.save(task);
            return  ResponseEntity.ok(task);
        } else {
            return ResponseEntity.status(409).body("Invalid date, e.g., \"dd HH:mm\"");
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateTask(@PathVariable("id") Integer id, @RequestBody Task task)
    {
        try {

            Task taskOhj = taskRepository.findById(id).orElseGet(null);
            String[] numberStringArray = taskOhj.getSubtaskId().split(",");
            List<Task> taskList = new ArrayList<>();

            // Adding tasks to a list
            for(String stringNumber : numberStringArray )
            {
                if(stringNumber.length() != 0)
                {
                    Integer dubTaskId = Integer.parseInt(stringNumber.trim());
                    taskList.add(taskRepository.findTaskByTaskId(dubTaskId));
                }
            }

            if(!TimeSpentFormatChecker.checkTimeSpentFormat(task.getTimeSpent()))
            {
                return ResponseEntity.status(409).body("Invalid date, e.g., \"dd HH:mm\"");
            }

            if(task.isTaskFinished())
            {
                if(TaskFilter.subTaskStatusCheck(taskList))
                {
                    return ResponseEntity.status(409).body("Sub task are not finished");

                } else {
                    task.setTaskId(id);
                    taskRepository.save(task);
                    return ResponseEntity.ok(task);
                }
            } else {
                task.setTaskId(id);
                taskRepository.save(task);
                return ResponseEntity.ok(task);
            }
        } catch(Exception e) {
            return ResponseEntity.status(404).body(null);
        }
    }


}
