package com.taskmanagementsystem.demo.dao;

import com.taskmanagementsystem.demo.entity.Task;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TaskRepository extends CrudRepository<Task, Integer>
{
   Task findTaskByTaskId(Integer id);
}
