package com.taskmanagementsystem.demo.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;


@Getter
@Setter
@ToString
@Entity
@Table(name="task")
@ApiModel(description = "All details about the task. ")
public class Task implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "task_id")
    @ApiModelProperty(notes = "The database generated task ID")
    private Integer taskId;

    @ApiModelProperty(notes = "The task name")
    private String taskName;

    @ApiModelProperty(notes = "The spent time on task. Format: \"dd HH:mm\" ")
    private String timeSpent;

    @ApiModelProperty(notes = " The task group ")
    private String taskGroup;

    @ApiModelProperty(notes = " The task assignees full name that do the task ")
    private String  taskAssigne;

    @Column(name = "sub_task_id")
    @ApiModelProperty(notes = " The sub task ID. Format: \"1,2,4,5\" different ID separated by comma")
    private String subtaskId;

    @ApiModelProperty(notes = " The task status that show if the task is finished")
    private boolean isTaskFinished;

}
