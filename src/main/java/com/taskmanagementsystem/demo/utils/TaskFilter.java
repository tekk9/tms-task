package com.taskmanagementsystem.demo.utils;

import com.taskmanagementsystem.demo.entity.Task;

import java.util.List;

public class TaskFilter
{
    public static boolean subTaskStatusCheck(List<Task> taskList)
    {
        for(Task taskListItem : taskList)
        {

            if( taskListItem != null && !taskListItem.isTaskFinished())
            {

                return true;
            }
        }
        return false;
    }

}


