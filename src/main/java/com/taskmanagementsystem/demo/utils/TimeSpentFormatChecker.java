package com.taskmanagementsystem.demo.utils;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class TimeSpentFormatChecker
{

    // String foramt checker Format("dd HH:mm")  ("00 00:00")
    public static boolean  checkTimeSpentFormat(String stringTime)
    {
        if(stringTime.length() == 8)
        {
            try
            {
                LocalTime localTime = LocalTime.parse(stringTime, DateTimeFormatter.ofPattern("dd HH:mm"));
                return true;
            } catch (Exception e)
            {
                // This can happen if you are trying to parse an invalid date, e.g., 25:19:12.
                return false;
            }
        } else {
            return false;
        }

    }
}
